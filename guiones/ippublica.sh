#===============================================================================
#
#       ARCHIVO: ippublica.sh
#
#           USO: ./ippublica.sh
#
#   DESCRIPCIÓN: Este guión devuelve la IP pública de la conexión a internet actual.
#
#         AUTOR: Byron Quezada(byque2013@gmail.com)
#       CREATED: 26/04/21 15:32:20
#         NOTAS: https://www.sitepoint.com/zsh-tips-tricks/#:~:text=%24(which%20bash)%20.-,Linux,the%20changes%20to%20take%20effect.
#      REVISION: 1.0.0
#===============================================================================
#!/bin/bash

curl http://ipecho.net/plain

