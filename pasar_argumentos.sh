#!/bin/bash
################################################################################
# pasar_argumentos.sh
# Este programa recibe dos argumentos, luego imprime el número de argumentos
# recibidos e imprime los dos primeros.
#
# Uso:
# pasar_argumentos.sh sol luna
#
# Un comando de shell y cualquier argumento pasado a este mismo comando
# aparece como variable de shell numerada.
################################################################################

argumentos_entrada=$# # Contador de argumentos de entrada
echo "$argumentos_entrada"

arg1=$1 # Primer argumento
echo "$arg1"
echo "$2"
