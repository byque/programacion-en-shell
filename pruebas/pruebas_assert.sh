#!/bin/bash
 source $1/externos/assert.sh

# Se espera que `pasar_argumentos.sh` escriba "2", "34" y "c3A" en stdout en
# diferentes líneas
assert "$1/pasar_argumentos.sh 34 c3A" "2\n34\nc3A"
# Se espera que `seq 3` imprima "1", "2" and "3" en diferentes líneas
assert "seq 3" "1\n2\n3"
# Se espera que el código de salida de `true` sea 0
assert_raises "true"
# Se espera que el código de salida de `false` sea 1
assert_raises "false" 1
# fin del paquete de pruebas
assert_end ejemplos
