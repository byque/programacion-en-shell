#!/bin/bash

vocales=(a e i o u)

# Comando for en estilo tradicional de Shell
for vocal in ${vocales[*]}; do
  echo $vocal;
done

limite=$1
# Comando for en estilo C
for (( i=0; i<$limite; i=i+1 )); do
  echo $i;
done
