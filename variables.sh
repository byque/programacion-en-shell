#!/bin/bash
###############################################################################
# Las variables no tienen tipo en shell y por convención los nombres de las
# variables usan minúsculas, mientras que las mayúsculas se usan para las
# variables constantes. La declaración no puede llevar espacios.
# texto = 'asdf' -> Error
# texto='asdf'   -> Correcto
###############################################################################

letra=y # Asigna la cadena "y" a la variable "letra"

texto='¡Hola mundo!' # Al usar espacios se debe usar comillas o apóstrofes

expansion="Una cadena y $texto" # Otras expansiones como las variables pueden
                                # ser expandidas en una asignación.

lista="$(ls -l ./)" # Resultados de un comando

resultado=$((5 * 7)) # Expansión aritmética

escape="\t\tUna cadena\n" # Secuencias de escape como tabuladores y nuevas
                          # líneas.

TEXTO_FIJO='Constante'
echo $texto $TEXTO_FIJO

num1=54
echo 'Echo requiere "" para imprimir variables'
echo '$num1 con' "''"
echo "$num1 con \"\""
